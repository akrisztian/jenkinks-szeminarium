package service;

import domain.Grade;
import domain.Homework;
import domain.Pair;
import domain.Student;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import repository.GradeXMLRepository;
import repository.HomeworkXMLRepository;
import repository.StudentXMLRepository;
import validation.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

class ServiceTest {

    public static Service service;

    @org.junit.jupiter.api.BeforeAll
    public static void setUp() {
        Validator<Student> studentValidator = new StudentValidator();
        Validator<Homework> homeworkValidator = new HomeworkValidator();
        Validator<Grade> gradeValidator = new GradeValidator();

        StudentXMLRepository fileRepository1 = new StudentXMLRepository(studentValidator, "students.xml");
        HomeworkXMLRepository fileRepository2 = new HomeworkXMLRepository(homeworkValidator, "homework.xml");
        GradeXMLRepository fileRepository3 = new GradeXMLRepository(gradeValidator, "grades.xml");

        service = new Service(fileRepository1, fileRepository2, fileRepository3);
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
    }

    @org.junit.jupiter.api.Test
    void findAllStudents() {
        assertNotNull(service.findAllStudents());
    }

    @org.junit.jupiter.api.Test
    void findAllHomework() {
        assertNotNull(service.findAllHomework());
    }

    @org.junit.jupiter.api.Test
    void saveStudent() {
        Student student = new Student("43", "Antal Alma", 222);
        int result = service.saveStudent(student.getID(), student.getName(), student.getGroup());
        assertEquals(0, result);
        service.deleteStudent(student.getID());
    }

    @org.junit.jupiter.api.Test
    void saveStudentInvalidGroup() {
        Student student = new Student("44", "Antal Alma", 999);
        int result = service.saveStudent(student.getID(), student.getName(), student.getGroup());
        assertEquals(1, result);
    }

    @org.junit.jupiter.api.Test
    @DisplayName("checking if homework save works")
    void saveValidHomework() {
        Homework hw = new Homework("77", "some easy homework", 6, 2);
        int result = service.saveHomework(hw.getID(), hw.getDescription(), hw.getDeadline(), hw.getStartline());
        assertEquals(0, result);
        //assertTrue(result == 1);
        service.deleteHomework(hw.getID());
    }

    @org.junit.jupiter.api.Test
    void saveInvalidHomework() {
        Homework hw = new Homework("77", "some easy homework", 1, 12);
        int result = service.saveHomework(hw.getID(), hw.getDescription(), hw.getDeadline(), hw.getStartline());
        assertEquals(0, result);
    }

    @org.junit.jupiter.api.Test
    void extendDeadlineValid() {
        int result = service.extendDeadline("1", 1);
        assertEquals(1, result);
    }

    @org.junit.jupiter.api.Test
    void extendDeadlineInvalid() {
        assertThrows(ValidationException.class, () -> service.extendDeadline("1", 53));
    }

    @org.junit.jupiter.api.Test
    void saveGrade() {
        Student student = new Student("43", "Antal Alma", 222);
        service.saveStudent(student.getID(), student.getName(), student.getGroup());
        Homework hw = new Homework("26", "some easy homework", 6, 2);
        service.saveHomework(hw.getID(), hw.getDescription(), hw.getDeadline(), hw.getStartline());


        Grade grade = new Grade(new Pair<String, String>(student.getID(), hw.getID()), 9, 2, "Nice");
        int result = service.saveGrade(
                grade.getID().getObject1(),
                grade.getID().getObject2(),
                grade.getGrade(),
                grade.getDeliveryWeek(),
                grade.getFeedback()
        );
        assertTrue(result == 0);

        service.deleteStudent(student.getID());
        service.deleteHomework(hw.getID());
    }

    @Test
    void assertAllTest(){
        Student s = new Student("99", "Johan", 533);
        assertAll(
                "student",
                () -> assertEquals("99", s.getID()),
                () -> assertEquals("Johan", s.getName())
        );
    }

    @ParameterizedTest
    @ValueSource(ints = {-10, 55, 533})
    void testStudentAddByGroup(int group){
        assumeTrue(group >= 0);
        Student s = new Student("99", "Johan", group);
        int result = service.saveStudent(s.getID(), s.getName(), s.getGroup());
        assertEquals(1, result);
        service.deleteStudent(s.getID());
    }
}